private void SelectionSort(long[] inputArray)
{
    long index_of_min = 0;
    for (int iterator = 0; iterator < inputArray.Length - 1; iterator++)
    {
        index_of_min = iterator;
        for (int index = iterator+1; index < inputArray.Length; index++)
        {
            if (inputArray[index] < inputArray[index_of_min])
                index_of_min = index;
        }
        Swap(ref inputArray[iterator], ref inputArray[index_of_min]);
    }
}